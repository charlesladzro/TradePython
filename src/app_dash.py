import traceback
from datetime import datetime

import dash
import dash_core_components as dcc
import dash_html_components as html
import krakenex
import pandas as pd
import plotly.graph_objects as go
from dash.dependencies import Input, Output
from pykrakenapi import KrakenAPI

# init
api = krakenex.API()
api.load_key('kraken.key')
k = KrakenAPI(api)

interval_temp = 1 * 1000
attente = 0
app = dash.Dash(__name__)
pair = "BTCEUR"
TimeIntervalMin = 1
ohlc, last, df_temp = [], datetime.now().timestamp() - 60 * 60 * TimeIntervalMin, []
nombre_de_passage = 0


def attente_seconde_0():
    global  attente
    attente = 1
    while (datetime.now().second != 3):
        pass
    attente = 0


def retriving_from_server(last_date):
    ohlc_, last_ = [], None
    ok = False
    while (not (ok)):
        try:
            ohlc_, last_ = k.get_ohlc_data(pair, interval=TimeIntervalMin, ascending=True, since=last_date)
            ok = True
        except KeyboardInterrupt:
            exit()

        except:
            print("\nErreur rencontré:")
            traceback.print_exc()
    return ohlc_, last_


def get_current_data():
    global ohlc, last, nombre_de_passage
    data = []

    print(len(ohlc))
    if len(ohlc) == 0:
        nombre_de_passage = nombre_de_passage + 1
        ohlc, last = retriving_from_server(last)

    if nombre_de_passage >= 2:
        data = ohlc
        ohlc = ohlc[len(ohlc):]
    else:
        data = ohlc.head(1)
        ohlc = ohlc[1:]

    return data


def display_candlestick(value):
    global df_temp, interval_temp, attente

    if attente == 0:
        # recuperation de la valeur suivante
        try:
            df_temp = pd.concat([df_temp, get_current_data()])
        except:
            df_temp = get_current_data()

    print(df_temp.tail(5))
    # mise à jour de l'affichage
    fig = go.Figure(go.Candlestick(
        x=df_temp.index,
        open=df_temp['open'],
        high=df_temp['high'],
        low=df_temp['low'],
        close=df_temp['close']
    ))
    style = {'padding': '5px', 'fontSize': '16px'}

    if nombre_de_passage == 2 and attente == 0:
        attente_seconde_0()
        interval_temp = 1 * 1000 * 60

    return [html.Span('Nombre de bougie: {0:.2f}'.format(df_temp.shape[0]), style=style)], fig, interval_temp


def set_layout():
    return html.Div([html.Div(id='live-update-text'), dcc.Graph(id="graph"),
                     dcc.Interval(id='interval-component', interval=1 * 1000),
                     dcc.Interval(id='period', interval=1 * 1000 * 60)])


app.layout = set_layout()
app.callback(Output('live-update-text', 'children'), Output("graph", "figure"),
             Output('interval-component', 'interval'),
             Input('interval-component', 'n_intervals'))(display_candlestick)

app.run_server(debug=True)
